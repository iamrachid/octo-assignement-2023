package ma.octo.assignement;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.UserRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class NiceBankApplication implements CommandLineRunner {
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private TransferRepository transferRepository;

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@Override
	public void run(String... strings) {
		User user1 = new User();
		user1.setUsername("user1");
		user1.setLastname("last1");
		user1.setFirstname("first1");
		user1.setGender("Male");
		user1.setPassword("$2a$12$4LUzstKutZ9e1aYV233D/uC3gLBxxBVhkw0v8G3ASdQUzI1ClFe7q");
		user1.setRole("ROLE_USER");

		userRepository.save(user1);

		User user2 = new User();
		user2.setUsername("user2");
		user2.setLastname("last2");
		user2.setFirstname("first2");
		user2.setGender("Female");
		user2.setRole("ROLE_USER");
		user2.setPassword("$2a$12$4LUzstKutZ9e1aYV233D/uC3gLBxxBVhkw0v8G3ASdQUzI1ClFe7q");

		userRepository.save(user2);

		Account account1 = new Account();
		account1.setAccountNumber("010000A000001000");
		account1.setRib("RIB1");
		account1.setBalance(BigDecimal.valueOf(200000L));
		account1.setUser(user1);

		accountRepository.save(account1);

		Account account2 = new Account();
		account2.setAccountNumber("010000B025001000");
		account2.setRib("RIB2");
		account2.setBalance(BigDecimal.valueOf(140000L));
		account2.setUser(user2);

		accountRepository.save(account2);

		Transfer t = Transfer.builder()
				.id(5L)
				.amount(BigDecimal.TEN)
				.checkingAccount(account1)
				.depositAccount(account2)
				.transactionDate(new Date())
				.reason("Assignment 2021")
				.build();

		transferRepository.save(t);

		Transfer t2 = Transfer.builder()
				.id(10L)
				.amount(BigDecimal.TEN)
				.checkingAccount(account1)
				.depositAccount(account2)
				.transactionDate(new Date())
				.reason("Assignment 2021")
				.build();

		transferRepository.save(t2);
	}
}
