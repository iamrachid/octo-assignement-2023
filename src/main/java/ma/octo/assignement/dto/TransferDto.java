package ma.octo.assignement.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@Getter
@NoArgsConstructor
public class TransferDto extends TransactionDto {
}
