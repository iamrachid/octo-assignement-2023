package ma.octo.assignement.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorColumn(name="type")
@DiscriminatorValue("deposit")
public class Deposit extends Transaction{

  @Column
  private String depositorFullName;
}
