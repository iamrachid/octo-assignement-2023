package ma.octo.assignement.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@DiscriminatorColumn(name="type")
@DiscriminatorValue("transfer")
public class Transfer extends Transaction{

  @ManyToOne
  private Account checkingAccount;
}
