package ma.octo.assignement.service;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;


public interface DepositService {
    List<Deposit> getAll();


    void saveDeposit(TransactionDto transactionDto) throws TransactionException, AccountNotFoundException;
}
