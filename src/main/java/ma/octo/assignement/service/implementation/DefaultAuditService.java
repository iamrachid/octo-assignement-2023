package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class DefaultAuditService implements AuditService {

    private final Logger logger = LoggerFactory.getLogger(DefaultAuditService.class);

    private final AuditRepository auditRepository;

    public DefaultAuditService(AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    @Override
    public void audit(String message, EventType type) {

        logger.info("Audit de l'événement {}", type);

        Audit audit = new Audit();
        audit.setEventType(type);
        audit.setMessage(message);
        auditRepository.save(audit);
    }
}
