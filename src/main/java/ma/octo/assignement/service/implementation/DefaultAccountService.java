package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.service.AccountService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DefaultAccountService implements AccountService {

    private final AccountRepository accountRepository;

    public DefaultAccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<Account> getAccounts() {
        return accountRepository.findAll();
    }
}
