package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.DefaultUserDetails;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class DefaultUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findClientByUsername(username);
        List<GrantedAuthority> authorities = Arrays
                .asList(new SimpleGrantedAuthority(user.getRole()));
        return new DefaultUserDetails(user.getUsername(), user.getPassword(), authorities);
    }
}
