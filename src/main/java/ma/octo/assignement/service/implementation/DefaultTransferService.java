package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransactionMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Service
@Transactional
public class DefaultTransferService implements TransferService {
    private final Logger logger = LoggerFactory.getLogger(DefaultTransferService.class);

    private final TransactionMapper transferMapper;


    private final TransferRepository transferRepository;

    private final AccountRepository accountRepository;

    private final AuditService auditService;

    public DefaultTransferService(TransactionMapper transferMapper, TransferRepository transferRepository, AccountRepository accountRepository, AuditService auditService) {
        this.transferMapper = transferMapper;
        this.transferRepository = transferRepository;
        this.accountRepository = accountRepository;
        this.auditService = auditService;
    }

    @Override
    public List<Transfer> getTransfers() {
        return transferRepository.findAll();
    }

    @Override
    public void createTransfer(TransactionDto transactionDto) throws SoldeDisponibleInsuffisantException, TransactionException, AccountNotFoundException {
        Transfer transfer = (Transfer) transferMapper.dtoToDomain(transactionDto);
        if (transfer.getCheckingAccount().getBalance().intValue() - transfer.getAmount().intValue() < 0) {
            logger.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Motif vide");
        }


        transfer.getCheckingAccount()
                .setBalance(transfer.getCheckingAccount()
                        .getBalance()
                        .subtract(transfer.getAmount()));
        accountRepository.save(transfer.getCheckingAccount());
        transferRepository.save(transfer);

        String message = new StringBuilder("Transfer depuis ")
                .append(transfer.getCheckingAccount())
                .append(" vers ")
                .append(transfer.getDepositAccount())
                .append(" d'un montant de ")
                .append(transfer.getAmount())
                .toString();

        auditService.audit(message, EventType.TRANSFER);
    }


}
