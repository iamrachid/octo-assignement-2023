package ma.octo.assignement.service.implementation;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransactionMapper;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.DepositService;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DefaultDepositService implements DepositService {

    private final DepositRepository depositRepository;

    private final TransactionMapper depositMapper;

    private final AuditService auditService;

    public DefaultDepositService(DepositRepository depositRepository, TransactionMapper depositMapper, AuditService auditService) {
        this.depositRepository = depositRepository;
        this.depositMapper = depositMapper;
        this.auditService = auditService;
    }


    @Override
    public List<Deposit> getAll() {
        return depositRepository.findAll();
    }

    @Override
    public void saveDeposit(TransactionDto transactionDto) throws TransactionException, AccountNotFoundException {
        Deposit deposit = (Deposit) depositMapper.dtoToDomain(transactionDto);
        depositRepository.save(deposit);
        String message = new StringBuilder("Transfer depuis ")
                .append(deposit.getDepositorFullName())
                .append(" vers ")
                .append(deposit.getDepositAccount())
                .append(" d'un montant de ")
                .append(deposit.getAmount())
                .toString();
        auditService.audit(message,EventType.DEPOSIT);
    }
}
