package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;

import java.util.List;


public interface AccountService {
    List<Account> getAccounts();
}
