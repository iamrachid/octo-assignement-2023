package ma.octo.assignement.service;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import java.util.List;

public interface TransferService {
    List<Transfer> getTransfers();

    void createTransfer(TransactionDto transactionDto) throws SoldeDisponibleInsuffisantException, TransactionException, AccountNotFoundException;

}
