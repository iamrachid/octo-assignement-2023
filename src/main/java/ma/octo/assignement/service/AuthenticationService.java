package ma.octo.assignement.service;

import org.springframework.security.core.userdetails.UserDetails;


public interface AuthenticationService {
    String authenticate(UserDetails userDetails);
}
