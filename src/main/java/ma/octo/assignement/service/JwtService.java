package ma.octo.assignement.service;

import org.springframework.security.core.userdetails.UserDetails;


public interface JwtService {
    String extractUsername(String jwt);

    String generateToken(UserDetails userDetails, int mins, boolean withclaims);

    boolean validateToken(String jwt, UserDetails userDetails);
}
