package ma.octo.assignement.service;

import ma.octo.assignement.domain.util.EventType;

public interface AuditService {
    void audit(String message, EventType type);
}
