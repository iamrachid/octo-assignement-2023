package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.dto.TransactionDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class DepositMapper extends TransactionMapper {

    @Override
    Transaction addDepositorToTransaction(Transaction transaction, String depositor) {
        Deposit deposit = (Deposit)transaction;
        deposit.setDepositorFullName(depositor);
        return deposit;
    }

    @Override
    void addDepositorToTransactionDTO(TransactionDto transactionDto, Transaction transaction) {
        Deposit deposit = (Deposit) transaction;
        transactionDto.setDepositor(deposit.getDepositorFullName());
    }

}