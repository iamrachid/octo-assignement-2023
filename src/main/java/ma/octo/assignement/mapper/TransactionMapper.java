package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class TransactionMapper {

    private static final int MAX_AMOUNT = 1000;
    private static final int MIN_AMOUNT = 10;
    @Autowired
    protected AccountRepository accountRepository;

    protected final Logger logger = LoggerFactory.getLogger(TransactionMapper.class);

    public Transaction dtoToDomain(TransactionDto transactionDto) throws AccountNotFoundException, TransactionException {
        Account receiver = accountRepository.findByAccountNumber(transactionDto.getDepositAccountNumber());
        if ( receiver == null){
            logger.error("Compte non existant");
            throw new AccountNotFoundException("Compte non existant");
        }

        if (transactionDto.getAmount().intValue() < MIN_AMOUNT) {
            logger.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        }

        if (transactionDto.getAmount().intValue() > MAX_AMOUNT) {
            logger.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        receiver.setBalance(receiver.getBalance()
                                    .add(transactionDto.getAmount()));
        accountRepository.save(receiver);

        Transaction transaction = Transaction.builder()
                .depositAccount(receiver)
                .transactionDate(transactionDto.getDepositDate())
                .amount(transactionDto.getAmount())
                .reason(transactionDto.getReason())
                .build();

        return addDepositorToTransaction(transaction,transactionDto.getDepositor());
    }

    abstract Transaction addDepositorToTransaction(Transaction transaction, String depositor) throws AccountNotFoundException;

    public TransactionDto domainToDto(Transaction transaction){

        TransactionDto transactionDto = TransactionDto.builder()
                .depositAccountNumber(transaction.getDepositAccount().getAccountNumber())
                .depositDate(transaction.getTransactionDate())
                .reason(transaction.getReason())
                .amount(transaction.getAmount())
                .build();

        addDepositorToTransactionDTO(transactionDto, transaction);

        return transactionDto;
    }

    abstract void addDepositorToTransactionDTO(TransactionDto transactionDto, Transaction transaction) ;
}
