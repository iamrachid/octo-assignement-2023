package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.exceptions.AccountNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class TransferMapper extends TransactionMapper {

    @Override
    Transaction addDepositorToTransaction(Transaction transaction, String depositor) throws AccountNotFoundException {
        Account sender = accountRepository.findByAccountNumber(depositor);
        if ( sender == null){
            logger.error("Compte non existant");
            throw new AccountNotFoundException("Compte non existant");
        }
        Transfer transfer = (Transfer)transaction;
        transfer.setCheckingAccount(sender);
        return transfer;
    }

    @Override
    void addDepositorToTransactionDTO(TransactionDto transactionDto, Transaction transaction) {
        Transfer transfer = (Transfer) transaction;
        transactionDto.setDepositor(transfer.getCheckingAccount().toString());
    }
}