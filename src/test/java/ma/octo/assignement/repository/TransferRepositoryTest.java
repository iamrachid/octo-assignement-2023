package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Transfer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class TransferRepositoryTest {

  @Autowired
  private TransferRepository transferRepository;

  @Test
  void findById() {
    Optional<Transfer> optional = transferRepository.findById(5L);
    assertTrue(optional.isPresent());
    Transfer transfer = optional.get();
    assertEquals(Optional.of(5L), transfer.getId());
  }

  @Test
  void findAll() {
    List<Transfer> transfers = transferRepository.findAll();
    assertEquals(2, transfers.size());
  }

  @Test
  void save() {

  }

  @Test
  void delete() {
  }
}